/*
 * A Battleship game program
 * Author: Craig Bentall, 10350172
 * April 15, 2012
 */
import java.util.Scanner;
import java.util.Random;
class bg9 {// Start of class

	public static void main(String args[])
	{// Start of main

		//Variable Declarations
		
		boolean shipPlaced  = false;
		boolean shotPlaced  = false;
		boolean isValidShot = false;
		boolean demoGame	= false;//Computer vs Computer game, if true Computer will play aganist itself

		char[][] 	playerGrid = produceGrid();
		char[][] 	comGrid = produceGrid();

		int x = 0;
		int y = 0;
		int choice;		
		int aiShotsTaken = 0;
		int shipPlacement;
		
		int[] hitCoord = new int[2];
		hitCoord[0] = -1;
		hitCoord[1] = -1;
		
		int[] targetCoord = new int[2];
				
		String fleet[] ={ //Creating the Fleet
            "Battleship",
            "Battleship",
            "Submarine",
            "Submarine",
        };
        
		getWelcomeMessage();// Method call to show welcome message	 
		String playerName = getPlayerName();// Method call to get players name        
		
		//Application Logic
        
		printGrid(printPlayerName(playerName), playerGrid);// Method call to generate a grid for the player		    
		printGrid(printComputerName(), comGrid);// Method call to generate a grid for the computer
		
		// Placing the player's ships
		
		for (shipPlacement = 0; shipPlacement < fleet.length; shipPlacement++) //for while theres ships still to place
	    {
	    	shipPlaced = false;
	    	
	    	while (!shipPlaced)// while ship not placed
	    	{
	    	
		    	x 		= getChoice("please choose x between 0-" + getUpperBoundary(playerGrid,'x') +" for your " + fleet[shipPlacement], 0, getUpperBoundary(playerGrid,'x'));// Storing the next int from the player in x
		    	y 		= getChoice("please choose y between 0-" + getUpperBoundary(playerGrid,'y') +"  for your " + fleet[shipPlacement], 0, getUpperBoundary(playerGrid,'y'));// Storing the next int from the player in y
				choice 	= getChoice("please choose 1 for horizontal or 2 for vertical for your " + fleet[shipPlacement], 1, 2);// Storing the next int from the player in choice
	
				if (shipFitsInGrid(x, y, getShipLength(fleet[shipPlacement]), playerGrid ))// checking to see if ship fits in grid
				{
	
					if (!shipPresent (x, y, choice, getShipLength(fleet[shipPlacement]), playerGrid))// checking to see if a ship is not already there
					{
						placeShip(x, y, choice, getShipLength(fleet[shipPlacement]), playerGrid);// place ship
						shipPlaced = true;
					}
					else
					{
						System.out.println("Error " + fleet[shipPlacement] + " already there, please try again");
					}
				}
				else
				{
					System.out.println("Error " + fleet[shipPlacement] + " out of bounds");				
				}
		
			}
	       
	    }
	
		printGrid(printPlayerName(playerName), playerGrid);
		
		// Placing the computer's ships
		
		for (shipPlacement = 0; shipPlacement < fleet.length; shipPlacement++)
	    {
	    	shipPlaced = false;
	    	
	    	while (!shipPlaced)// while ship not placed
	    	{
	    	
		    	x 		= getRandomNumber(0, getUpperBoundary(comGrid,'x'));// get random numbers
		    	y 		= getRandomNumber(0, getUpperBoundary(comGrid,'y'));
				choice 	= getRandomNumber(1, 2);
	
				if (shipFitsInGrid(x, y, getShipLength(fleet[shipPlacement]), comGrid ))// checking to see if ship fits in grid
				{
	
					if (!shipPresent (x, y, choice, getShipLength(fleet[shipPlacement]), comGrid))// checking to see if a ship is not already there
					{
						placeShip(x, y, choice, getShipLength(fleet[shipPlacement]), comGrid);// place ship
						shipPlaced = true;
					}
				}
		
			}
	       
	    }
		printGrid(printComputerName(), comGrid);
		
		//Taking the shots
		
		while (shipsRemaining(playerGrid, comGrid))
		{
		    //Players shots
		    
		    shotPlaced = false;
		    
			while(!shotPlaced)
			{
			
			if (demoGame)// demoGame code 
			{
				
		    	x 	= getRandomNumber(0, getUpperBoundary(comGrid,'x'));// generate random number 
		    	y 	= getRandomNumber(0, getUpperBoundary(comGrid,'y'));
		    }
		    else// get user input
		    {
					
		    	
		    	x 		= getChoice("please choose x between 0-" + getUpperBoundary(comGrid,'x') +" for your shot", 0, getUpperBoundary(comGrid,'x'));// getting x for shot
		    	y 		= getChoice("please choose y between 0-" + getUpperBoundary(comGrid,'y') +" for your shot", 0, getUpperBoundary(comGrid,'y'));// getting y for shot	
		    	
		    }	
		    	
		    	
		    	if (isValidShot(x, y, comGrid))// checking to see if valid shot
		    	{
		    		if (!hasBeenHit(x, y, comGrid))// checking to see if a shot has already been placed there 
		    		{
		    			comGrid = placeShot(x, y, comGrid);
		    			checkShot(x, y, comGrid, playerName);
						printGrid(printComputerName(), comGrid);
		    			shotPlaced = true;// change shotPlaced to true
		    		}
		    	}
		    }
		    
		    
		    if (playerWon(comGrid))// check to see if anyones won
		    {
		    	announceWinner(playerName, printComputerName());// get winners names
		    	break;	
		    }
		    	
	    	//Computers shots
	    	
		    shotPlaced = false;
		    
			while(!shotPlaced)
			{

				if (hitCoord[0] > -1 && hitCoord[1] > -1)// checking for any hitCoords 
				{
					targetCoord = generateTargetCoord(hitCoord[0], hitCoord[1]);
					x = targetCoord[0];
					y = targetCoord[1];
					System.out.println("Using Hit, x: '" + x + "', y: '" + y + "'");
					aiShotsTaken++;
				}
				else
				{
			    	x 	= getRandomNumber(0, getUpperBoundary(playerGrid,'x'));
			    	y 	= getRandomNumber(0, getUpperBoundary(playerGrid,'y'));
		    	}

	    	
		    	if (isValidShot(x, y, playerGrid))// checking to see if valid shot
		    	{
		    		
		    		if (!hasBeenHit(x, y, playerGrid)) // checking to see if a shot has already been placed there 
		    		{	    			
		    			playerGrid = placeShot(x, y, playerGrid);
		    			if (checkShot(x, y, playerGrid, printComputerName()))
		    			{
		    				hitCoord[0] = x;// storing a hit coordinate for computer AI shot
		    				hitCoord[1] = y;
		    			}
		    			
						printGrid(printPlayerName(playerName), playerGrid);
		    			shotPlaced = true;

		    		}
		    		else if(aiShotsTaken > 4) // resetting hitCoord
		    		{
		    			hitCoord[0] = -1;
		    			hitCoord[1] = -1;
		    			aiShotsTaken = 0;
		    		}
		    		
		    	}

		    }
		    
		    // Game Over
		    
		    if (playerWon(playerGrid))
		    {
		    	announceWinner(printComputerName(), playerName);
		    	break;	
		    }		    	    	
		}    
    
    }// End of main
    
    // Methods

	static char[][] produceGrid()// Produce a grid of chars
	{
		char grid[][] ={
            {'0','0','0','0','0','0','0','0','0','0'},
            {'0','0','0','0','0','0','0','0','0','0'},
            {'0','0','0','0','0','0','0','0','0','0'},
            {'0','0','0','0','0','0','0','0','0','0'},
            {'0','0','0','0','0','0','0','0','0','0'},
            {'0','0','0','0','0','0','0','0','0','0'},
            {'0','0','0','0','0','0','0','0','0','0'},
            {'0','0','0','0','0','0','0','0','0','0'},            
            {'0','0','0','0','0','0','0','0','0','0'},
            {'0','0','0','0','0','0','0','0','0','0'},
            {'0','0','0','0','0','0','0','0','0','0'},
            {'0','0','0','0','0','0','0','0','0','0'}

            };
          
          return grid;
	}
	
	static int getChoice(String msg, int lowerBound, int upperBound)//method to get a choice
	{
		int number =getInt(msg);
		
		while (number <lowerBound || number > upperBound )
		{
			System.out.println("Sorry, " + number + " is not a valid choice");
			number =getInt(msg);

		}
		return number;
	}
	
	static int getInt (String msg)//method to collect the user�s next int input
	{
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println();
    	System.out.println(msg);//telling the player what to do
    	return myScanner.nextInt();// storing the next int from the player in choice				
	}
	
	static int getUpperBoundary (char[][] grid, char axis)//A method to check the grid column size and the grid row size 
	{
		
		int upperBoundary = 0;
		if(axis == 'x')
		{
			upperBoundary = (grid.length -1);// check column size
		}
		else if(axis =='y')
		{
			upperBoundary = (grid[0].length -1);// check row size			
		}
		return upperBoundary;
	}
	
	static char[][] placeShip(int x, int y, int choice, int shipLength, char[][] grid)//: A method to determine which way to place the ship 
	{
		int i;

   		for(i = 0; i < shipLength; i++)
    	{
    		if (choice == 1)
    		{
    			grid[x][y+i] = 's';	    			
    		}
    		else if (choice == 2)
    		{
    			grid[x+i][y] = 's';	    			
    		}
    	}

	    return grid;
	}
	
	static boolean shipPresent(int x, int y, int choice, int shipLength, char[][] grid)//A method to check if a ship has already been placed at the coordinates 
	{
		if (choice ==1)
		{
			if (grid[x][y] !='s' && grid[x][y+1] !='s' && grid[x][y+2] !='s')
			{
				 return false;
			}
			
		}
		else if (choice ==2)
		{
			if (grid[x][y] !='s' && grid[x+1][y] !='s' && grid[x+2][y] !='s')
			{
				return false;
			}
		}
		
		return true;
	}

	static void printGrid(String gridType, char[][] grid)//A method to print a grid
	{
		boolean hideComputersShips = true;
		
		int x = 0;
		int y = 0;
			
		System.out.println();
		System.out.println(gridType + " Grid");
  	    for (x = 0;x < grid.length;x++)
  	    {
 	    	System.out.println("");
  	    	for (y=0;y < grid[x].length;y++)
    		{
       			 if (hideComputersShips) 
       			 {
	       			 if(gridType == printComputerName())
	       			 {
	       			 	if(grid[x][y] == 's')
	       			 	{
	       			 		System.out.print('0');
	       			 	}
	       			 	else
	       			 	{
	       			 		System.out.print(grid[x][y]);
	       			 	}
	       			 }
	       			 else 
	       			 {
	       			 	System.out.print(grid[x][y]);
	       			 }
	       		}
	       		else
	       		{
	       			System.out.print(grid[x][y]);	       			
	       		}
       			 
    		 	
   			}
		}
		System.out.println();		
    }
    
    static void getWelcomeMessage()//A method to print out a welcome message to the screen
    {
    	
    	System.out.println("Welcome to Battleships");
    	System.out.println();
    	System.out.println("A naval warfare game");
    	System.out.println("Where you, a commander of your own fleet of ships");
    	System.out.println("must fight to the death against Zeus the evil");
    	System.out.println("commander of the computers fleet");
    	System.out.println();
    }
    
    static String getPlayerName()//A method to get the next String input from user and store as playerName
    {
    	String playerName;
    	
    	System.out.print("Please enter your name ");
    	
    	Scanner myScanner = new Scanner(System.in);
     	playerName = myScanner.nextLine();
     	return playerName;
    }
    
    static String printPlayerName(String playerName)//A method to return the String playerName
    {
    	return playerName + "'s";
    }
    
    static String printComputerName()//A method to return a String Zeus
    {
    	return "Zeus";
    }
    
    static int getShipLength (String shipType)//A method that gets a String shipType and checks to see if its equal to Battleship or Submarine
    {
    	int shipLength = 0;
    	if (shipType == "Battleship")
    	{
   			shipLength = 4;
    	}
    	else if (shipType == "Submarine")
    	{
    		shipLength = 3;
    	}
    	return shipLength;
    }
    
    static boolean shipFitsInGrid (int x, int y, int shipLength, char[][] grid)//A method that checks to see if the ship fits within the size of the grid 
    {
	    if (x + shipLength <= grid.length && y + shipLength <= grid[0].length)
	    {
	    	return true;    		    
	    }
	    else
	    {
			return false;		    	
	    }
    }

    static int getRandomNumber(int min, int max)//A method that generates a random number 
    {
		int randomNumber = (min + (int)(Math.random() * ((max - min) + 1)));
		return randomNumber;    	
    }
    
    static boolean shipsRemaining(char[][] playerGrid, char[][] comGrid)//A method that checks playerGrid and comGrid for any ships remaining on the grids 
    {
    	boolean shipsRemaining = false;
  	    
  	    int x = 0;
  	    int y = 0;
  	    
  	    for (x = 0;x < playerGrid.length;x++)
  	    {
  	    	for (y = 0;y < playerGrid[x].length;y++)
    		{
       			 if (playerGrid[x][y] == 's')
       			 {
       			 	return true;
       			 }
   			}
		}
		
  	    for (x = 0;x < comGrid.length;x++)
  	    {
  	    	for (y = 0;y < comGrid[x].length;y++)
    		{
       			 if (comGrid[x][y] == 's')
       			 {
       			 	return true;
       			 }
   			}
		}		    	
    	
    	
    	return shipsRemaining;
    }
    
    static boolean isValidShot(int x, int y, char[][] grid)//A method to check whether the shot is within the grid
    {    	
    	if ((x >= 0 && x <= getUpperBoundary(grid, 'x')) && (y >= 0 && y <= getUpperBoundary(grid, 'y')))
    	{
    		return true;
    	}
    	
    	System.out.print("Sorry, invalid shot. Please shot between 0 and " + getUpperBoundary(grid,'x') + " for X, and between 0 and " + getUpperBoundary(grid,'y') + " for y");
    	return false;
    }
    
    static boolean hasBeenHit(int x, int y, char[][] grid)//A method to check whether a point on the grid has been hit before
    {
    	if (grid[x][y] == 'h' || grid[x][y] == 'm')
    	{
    		System.out.println("Sorry already been a shot there");
    		return true;
    	}
    	
    	return false;
    }
    
    static int[] generateTargetCoord(int x, int y)//A method to generate a nextGuess int. This will be used for the computer AI 
    {
    	int[] nextGuess =  new int[2];
    	int axis = getRandomNumber(1,2);
    	int direction = getRandomNumber(1,2);
    	
    	if(axis == 1)
    	{
    		switch(direction)
    		{
    			case 1:
    			nextGuess[0] = (x-1);
    			break;
    			
    			case 2:
    			nextGuess[0] = (x+1);
    		}
    		nextGuess[1] = y;
    	}
    	
    	if(axis == 2)
    	{
    		switch(direction)
    		{
    			case 1:
    			nextGuess[1] = (y-1);
    			break;
    			
    			case 2:
    			nextGuess[1] = (y+1);
    			break;
    		}
    		nextGuess[0] = x;
    	}
		return nextGuess;    	
    }
    
    static char[][] placeShot(int x, int y, char[][] grid)//A method to place a shot within the grid and represent it with a value �h� or �m�
    {
    	if(grid[x][y] == 's')
    	{    	
    		grid[x][y] = 'h';
    	}
    	else if (grid[x][y] == '0')
    	{
    		grid[x][y] = 'm';
    	}
    	return grid;
    }
    
    static boolean checkShot(int x, int y, char[][] grid, String playerName)//A method to check the shot status
    {
    	char shotStatus = grid[x][y];
    	
    	if (shotStatus == 'h')
    	{
    		registerHit(playerName);
    		return true;
    	}
    	else if (shotStatus == 'm')
    	{
    		registerMiss(playerName);
    	}
    	return false;
    }
    
    static void registerHit(String playerName)//A method with a String array of hitMessages and prints a random one to the screen followed by the players name
    {
    	String[] hitMessages = {
    		"Wooo nice hit ",
    		"Excellent shot ",
    		"Nice one "
    	};
    	System.out.println(hitMessages[getRandomNumber(0,2)] + playerName);
    }
    
    static void registerMiss(String playerName)//A method with a String array of missMessages and prints a random one to the screen followed by players name
    {
    	String[] missMessages = {
    		"Nothing but fishes ",
    		"You couldn't hit a barn door ",
    		"Oh so unlucky "
    	};
  		System.out.println(missMessages[getRandomNumber(0,2)] + playerName);
    }
    
    static boolean playerWon(char[][] grid)//A method to determine if a player has won the game
    {
  	    int x = 0;
  	    int y = 0;
  	    
  	    for (x = 0;x < grid.length;x++)
  	    {
  	    	for (y=0;y < grid[x].length;y++)
    		{
       			 if (grid[x][y] == 's')
       			 {
       			 	return false;
       			 }
   			}
		}
		
		return true;    	
    }
    
    static void announceWinner(String winner, String loser)//A method to print a String for winner and a String for loser
    {
    	System.out.print(winner + " beat " + loser);
    }
   
}// End of class